(function(){
    angular
        .module("ShoppingCartApp")
        .service("sessionService", sessionService);

    sessionService.$inject = ["$http", "$q"];

    function sessionService($http, $q) {

        var vm = this;

        // Exposed functions
        vm.viewCart = viewCart;
        vm.addToCart = add;
        vm.checkout = _delete;

        // Function definition
        function add(newItem) {
            var defer = $q.defer();
            $http.post("/api/cart", { item: newItem })
                .then(function() 
                { defer.resolve() 
                });
            return (defer.promise);
        }

        function _delete() {
            var defer = $q.defer();
            $http.delete("/api/cart")
                .then(function() { 
                    defer.resolve() 
                });
            return (defer.promise);
        }

        function viewCart() {
            var defer = $q.defer();
            $http.get("/api/cart")
                .then(function(results) {
                    defer.resolve(results.data);
                });
            return (defer.promise);
        }
    }
})();
