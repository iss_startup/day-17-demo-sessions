var express = require("express");
var bodyParser = require("body-parser");
var session = require("express-session");
var routes = require("./routes");
var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(session({
    secret: "1111",
    resave: false,
    saveUninitialized: true
}));

app.use(function (req, res, next) {
    if (!req.session.cart) {
        req.session.cart = [];
    }
    next();
    
})

routes.init(app); // this is a more familiar pattern than > require("./routes")(app)
routes.errorHandler(app); // errorHandler also defined

//Start the web server on port 3000
app.listen(3000, function() {
    console.info("Webserver started on port 3000");
});